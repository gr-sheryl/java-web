package com.company.dao;

import com.company.pojo.Teacher;
import com.company.util.mybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * author: Sheryl
 * create time: 2022/4/7 9:21 下午
 */
public class MyTest {
    @Test
    public void getTeacherTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
            List<Teacher> teacherList = mapper.getTeacher2(1);
            for (Teacher teacher: teacherList){
                System.out.println(teacher);
            }
        }
    }
}
