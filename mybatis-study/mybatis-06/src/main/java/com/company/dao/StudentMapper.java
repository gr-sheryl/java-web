package com.company.dao;

import com.company.pojo.Student;
import com.company.pojo.Teacher;
import lombok.Data;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * author: Sheryl
 * create time: 2022/4/6 9:54 下午
 */
public interface StudentMapper {
    //查询所有学生，以及对应的老师的信息（笛卡尔积）
    public List<Student> getStudent();

    public List<Student> getStudent2();

}
