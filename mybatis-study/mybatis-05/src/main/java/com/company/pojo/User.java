package com.company.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * author: Sheryl
 * create time: 2022/4/3 4:53 下午
 */

@Data
@AllArgsConstructor
//实体类
public class User {
    private int id;
    private String name;
    private String password;
}
