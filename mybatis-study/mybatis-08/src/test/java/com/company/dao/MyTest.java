package com.company.dao;

import com.company.pojo.Blog;
import com.company.util.IDUtils;
import com.company.util.mybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.*;

/**
 * author: Sheryl
 * create time: 2022/4/8 4:09 下午
 */
public class MyTest {
    @Test
    public void addBlogTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Blog blog = new Blog();
            blog.setId(IDUtils.getId());
            blog.setAuthor("小河");
            blog.setTitle("c#");
            blog.setCreateTime(new Date());
            blog.setViews(2653);
            int i = mapper.addBlog(blog);
            System.out.println(i);
            sqlSession.commit();
        }
    }

    @Test
    public void queryBlogIFTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Map map = new HashMap();
            map.put("title", "mybatis");
            map.put("author","小李");
            List<Blog> blogs = mapper.queryBlogIF(map);
            for (Blog b : blogs){
                System.out.println(b);
            }
        }
    }

    @Test
    public void updateBlogIFTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Map map = new HashMap();
            map.put("title", "mybatis");
            //map.put("author","小张");
            map.put("id","73d36677f5dc41da9618e6d1eedac832");
            int blogs = mapper.updateBlog(map);
        }
    }

    @Test
    public void queryBlogForeachTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Map map = new HashMap();
            ArrayList<Integer> ids = new ArrayList<>();
            ids.add(1);
            ids.add(2);
            ids.add(3);

            map.put("ids", ids);
            List<Blog> blogs = mapper.queryBlogForeach(map);
            for (Blog b : blogs){
                System.out.println(b);
            }
        }
    }
}
