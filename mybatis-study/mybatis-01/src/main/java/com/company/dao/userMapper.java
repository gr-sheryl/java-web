package com.company.dao;
import com.company.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * author: Sheryl
 * create time: 2022/4/3 4:58 下午
 */
public interface userMapper {
    //查询全部用户
    List<User> getUserList();

    //根据ID查询用户
    User getUserByID(int id);

    //根据map查询用户
    User getUserByID2(Map<String, Object> map);

    //insert
    int addUser(User user);

    //update
    int updateUser(User user);

    //delete
    int deleteUser(int id);

    //使用map作为参数
    int addUser2(Map<String, Object> map);

    //模糊查询
    List<User> getUserLike(String value);

}
