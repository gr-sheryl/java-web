package com.company.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * author: Sheryl
 * create time: 2022/4/9 3:25 下午
 */
@Data
@AllArgsConstructor
public class User {
    private int id;
    private String name;
    private String pwd;
}
