package com.company.servlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * author: Sheryl
 * create time: 2022/3/26 2:54 下午
 */
public class LogFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        Object user_session = req.getSession().getAttribute("USER_SESSION");
        if(user_session == null){
            resp.sendRedirect(req.getContextPath() + "/error.jsp");
        }
        filterChain.doFilter(req, resp);
    }

    @Override
    public void destroy() {

    }
}
