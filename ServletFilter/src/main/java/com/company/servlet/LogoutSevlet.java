package com.company.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * author: Sheryl
 * create time: 2022/3/26 2:29 下午
 */
public class LogoutSevlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object user_session = req.getSession().getAttribute("USER_SESSION");
        if(user_session != null){
            req.getSession().removeAttribute("USER_SESSION");
            resp.sendRedirect(req.getContextPath() + "/login.jsp");
        }
    }
}
